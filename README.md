eziod
=====

EZIOd is a daemon which controls the EZIO-300 LCD panel and buttons.

 * git clone git@gitlab.com:clearos/clearfoundation/eziod.git
 * cd eziod
 * git checkout master

**modify and bump**

 * git commit -a
 * git push origin master


**test and repeat**

 * git checkout clear7
 * git merge master
 * git push clear7

**build**